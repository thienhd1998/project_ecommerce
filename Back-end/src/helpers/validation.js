import Joi from "@hapi/joi";

// Register Validation
export const registerValidation = data => {
    const schema = Joi.object().keys({   
        name: Joi.string()
            .min(6)
            .required(),     
        email: Joi.string()
            .min(6)
            .required()
            .email(),
        password: Joi.string()
            .min(6)
            .required()
    });
    return schema.validate(data, { abortEarly: false });
};

export const loginValidation = data => {
    const schema = Joi.object().keys({
        email: Joi.string()
            .min(6)
            .required()
            .email(),
        password: Joi.string()
            .min(6)
            .required()
    });
    return schema.validate(data, { abortEarly: false });
}
