import jwt from "jsonwebtoken";
import createError from "http-errors";
import { client } from '../helpers/init_redis';

require('dotenv').config(); 

export const signAccessToken = (payload) => {
    return new Promise((resolve, reject) => {
        const secret = process.env.TOKEN_SECRET;
        const options = {
            expiresIn: '1h' ,                       
        };

        jwt.sign(payload, secret, options, (err, token) => {
            if (err) {
                reject(createError.InternalServerError());
            };

            resolve(token);
        });
    });
};

export const signRefreshToken = (payload) => {
    return new Promise((resolve, reject) => {
        const secret = process.env.REFRESH_TOKEN_SECRET;
        const options = {
            expiresIn: '1y'                      
        };

        jwt.sign(payload, secret, options, (err, token) => {
            if (err) {
                reject(createError.InternalServerError());
            };
                resolve(token)                        
        });
    })
};

export const verifyRefreshToken = (refreshToken) => {
    return new Promise(async (resolve, reject) => {
        const userId =  await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
        if (!userId) return reject(createError.Unauthorized());

        resolve(userId);
    })
};

export const verifyToken = (req, res, next) => {    
    const authHeader = req.header('Authorization');
    const bearerToken = authHeader.split(' ');
    const token = bearerToken[1];
    
    if (!token) {
        res.status(401).json({
            success: false,
            message: 'Truy cập bị từ chối' 
        })
    }
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};