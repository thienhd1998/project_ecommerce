import mongoose from "mongoose";

const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    id: { type: String, default: null },
    email: { type: String, default: null },
    name: { type: String, default: null },
    phone: { type: Number, default: null },
    address: { type: String, default: null },
    status: { type: Number, default: 1 },
    total: { type: Number, default: null },
    cartItems: [{
        name: { type: String, default: null },
        quantity: { type: Number, default: null },
        price: { type: Number, default: null },        
    }],
    createdAt: { type: Number, default: Date.now() },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null }
});

const orderModel = mongoose.model("Order", OrderSchema);

export default orderModel;