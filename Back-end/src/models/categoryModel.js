import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    id: { type: Number, default: null },
    name: { type: String, default: null },
    childrens: [
        {
            id: { type: Number, default: null },
            name: { type: String, default: null }
        }
    ],
    createdAt: { type: Number, default: Date.now() },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null } 
});

const categoryModel = mongoose.model("Category", CategorySchema);

export default categoryModel;