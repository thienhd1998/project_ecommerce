import mongoose from "mongoose";
import bcrypt from "bcrypt";

const Schema = mongoose.Schema;

const UserSchema = new Schema({    
    username: String,
    email: { type: String, required: true, max: 255, min: 6 },   
    password: { type: String, required: true, max: 1024, min: 6 },
    isAdmin: { type: Boolean, required: false, default: false },    
    gender: {type: String, default: "male"},
    phone: {type: String, default: null},
    address: {type: String, default: null},
    avatar: {type: String, default: "default_avatar.jpg"},
    createdAt: { type: Number, default: Date.now() },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null }    
});

const userModel = mongoose.model("User", UserSchema);

export default userModel;