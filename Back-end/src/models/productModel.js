import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ProductSchema = new Schema({ 
    id: { type: Number, default: null },
    category: { type: String, default: null },
    brand: { type: String, default: null },
    name: { type: String, default: null },
    images: { type: Array, default: [] },
    price: { type: Number, default: 0 },
    inStock: { type: Boolean, default: true },
    sex: { type: String, default: "male" },
    size: [
        { 
            number: { type: Number, default: 0 },
            inStock: { type: Boolean, default: true }
        }
    ],
    promotion: { type: Number, default: 0 },
    description: { type: String, default: null },
    rating: { type: Number, default: 0 },
    numberReviews: { type: Number, default: 0 },    
    createdAt: { type: Number, default: Date.now() },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null }    
});

const productModel = mongoose.model("Product", ProductSchema);

export default productModel;