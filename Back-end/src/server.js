require('dotenv').config();

import express from "express";
import bodyParser from "body-parser";
import initRoutes from "./routes/index"; 
import ConnectDB from "./config/connectDB";
import createError from "http-errors";
import morgan from "morgan";
// import { client } from './helpers/init_redis';

// client.SET("foo", "bar")

// client.GET("foo", (err, value) => {
//     if (err) console.log(err)
//     console.log(value)
// });

const app = express();

ConnectDB();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));

// Handling erros:
// app.use(async (req, res, next) => {    
//     next(createError.NotFound())
// });

// app.use(async (err, req, res, next) => {
//     res.status(err.status || 500)
//     res.send({
//         error: {
//             status: err.status || 500,
//             message: err.message,
//         }
//     })
// });

// Handling CORS:
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"        
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, PATCH, DELETE");
        return res.status(200).json({});
    };
    next();
});

initRoutes(app);

app.listen(process.env.port, process.env.hostname, () => {
    console.log(`Hello Thien, I'm running at ${process.env.hostname}:${process.env.port}/`);
});
