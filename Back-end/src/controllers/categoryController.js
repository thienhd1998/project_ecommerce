import Category from '../models/categoryModel';

// Lấy danh sách category
export const getCategory = async (req, res, next) => {
    try {
        const category = await Category.find({});
        res.status(200).json({
            success: true,
            data: category
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

// Tạo danh sách category
export const createCategory = async (req, res, next) => {
    const { id, name, childrens } = req.body;
    const category = new Category({
        id: id,
        name: name,
        childrens: childrens
    });

    try {
        const newCategory = await category.save();
        res.status(200).json({
            success: true,
            message: "Tạo mới category thành công",
            data: newCategory
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Tạo mới category thất bại"
        })
    }
}

// Sửa danh sách category
export const updateCategory = async (req, res, next) => {
    try {
        const updatedCategory = await Category.updateOne(
            { id: req.query.id },
            { $set: req.body }
        );
        res.status(200).json({
            success: true,
            message: "Cập nhật category thành công",
            data: updatedCategory  
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Cập nhật category thất bại"
        })
    }
};

// Xoá danh sách category
export const deleteCategory = async (req, res, next) => {
    try {
        const deletedCategory = await Category.findOneAndDelete({ id: req.query.id });
        res.status(200).json({
            success: true,
            message: "Xoá sản phẩm thành công",
            data: deletedCategory
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Xoá sản phẩm thất bại"            
        })
    }
};