import multer from "multer";
import { file_app } from "../config/file_app";
import { v4 as uuidv4 } from 'uuid';
import fsExtra from "fs-extra";
import User from "../models/userModel";

// Kho dữ liệu avatar
const storagevAvatar = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, file_app.avatar_directory);
    },
    filename: (req, file, callback) => {
        let math = file_app.avatar_type;
        if (math.indexOf(file.mimetype) === -1 ) {
            return callback("Kiểu file không hợp lệ, chỉ chấp nhận jpg & png", null)
        };

        const avatarName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
        callback(null, avatarName);
    }
});

// File avatar upload
const avatarUploadFile = multer({
    storage: storagevAvatar,
    limits: { 
        fileSize: file_app.avatar_limit_size
    }
}).single("avatar");

// Cập nhật avatar:
export const updateAvatar = (req, res) => {
    avatarUploadFile(req, res, async (error) => {
      if (error) {
        if (error.message) {
          return res.status(500).json({
              success: false,
              message: "Ảnh tối đa cho phép là 1MB"
          });
        }
        return res.status(500).json({
            success: false,
            message: error.message
        });
      }
      try {
        let updateUserItem = {
          avatar: req.file.filename,
          updatedAt: Date.now()
        };
        // Update user
        let userUpdate = await User.updateOne({ _id: req.user._id, updateUserItem });
  
        // Remove old user avatar 
        await fsExtra.remove(`${file_app.avatar_directory}/${userUpdate.avatar}`);
   
        return res.status(200).json({ 
            success: true,
            message: "Cập nhật avatar thành công",
            data: {
                imageSrc: `/images/users/${req.file.filename}`
            }
        })
      } catch (error) {
        console.log(error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
      }
    });
  };