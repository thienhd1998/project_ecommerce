import Order from "../models/orderModel";
import User from "../models/userModel";

export const createOrder = async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });

    if (!req.body.total || !req.body.price) {
        return res.status(500).json({
            success: false,
            message: 'Đặt hàng thất bại'
        })
    }

    const order = new Order({
        name: user.username,
        email: user.email,
        phone: user.phone,
        address: user.address,
        total: req.body.total,
        cartItems: req.body.total
    });
    try {
        const newOrder = await order.save();
        res.status(200).json({
            success: true,
            message: 'Đặt hàng thành công',
            data: newOrder
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }    
};

export const createOrderForAnonymous = async (req, res, next) => {
    if (!req.body.name || !req.body.email || !req.body.phone || !req.body.address || !req.body.total || !req.body.cartItems) {
        return res.status(500).json({
            success: false,
            message: 'Đặt hàng thất bại'
        })
    }

    const order = new Order({
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,
        total: req.body.total,
        cartItems: req.body.cartItems
    })

    try {
        const newOrder = await order.save();
        res.status(200).json({
            success: true,
            message: 'Đặt hàng thành công',
            data: newOrder
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

export const getOrder = async (req, res, next) => {    
    try {
        const orders = await Order.find({});
        res.status(200).json({
            success: true,
            data: orders
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

export const getOrderDetail = async (req, res, next) => {
    try {
        const id = req.query.id;
        const orderDetail = await Order.findOne({ id: id });
        res.status(200).json({
            success: true,
            data: orderDetail
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

export const updateOrder = async (req, res, next) => {
    try {   
        const updatedOrder = await Order.updateOne(
            { id: req.query.id },
            { $set: req.body }
        );
        res.status(200).json({ 
            success: true,
            message: "Cập nhật đơn thành công",
            data: updatedOrder
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Cập nhật đơn hàng thất bại"
        })
    }
}

export const deleteOrder = async (req, res, next) => {
    try {
        const deletedOrder = await Order.findOneAndDelete({ id: req.query.id });

        res.status(200).json({
            success: true,
            message: "Xoá đơn hàng thành công",
            data: deletedOrder
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Xoá đơn hàng thất bại"
        })
    }
};