import User from '../models/userModel';
import { registerValidation, loginValidation } from "../helpers/validation";
import { signAccessToken, signRefreshToken, verifyRefreshToken } from '../helpers/jwt_helper';
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import createError from 'http-errors';
import sql from 'mssql';
import { dbConnection } from '../config/dbConfig';


require('dotenv').config();

export const register = async (req, res, next) => {
    const { error } = registerValidation(req.body);
    if (error) {
        res.status(400).json({
            success: false,
            message: error.details[0].message
        });
    }

    try {
        await sql.connect(dbConnection())        
        const emailUser = await sql.query(`select email from [User] where email='${req.body.email}'`);                
        if (emailUser.recordset[0] != undefined) {
            res.status(400).json({
                success: false,
                message: 'Email đã tồn tại'
            });
        };

        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        await sql.query(`INSERT INTO [User] (username, email, pass_word, isAdmin, name, address, phone, created_at, updated_at) VALUES (N'${req.body.name}', '${req.body.email}', '${hashedPassword}', '1', '', '', '', '1599539974', '1599539974')`)        
        
        res.status(200).json({
            success: true,
            message: "Đăng ký tài khoản thành công"
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export const login = async (req, res, next) => {
    try {
        const { error } = loginValidation(req.body);
        if (error) {
            res.status(400).json({
                success: false,
                message: error.details[0]
            })
        };
        await sql.connect(dbConnection())

        // Checking email
        const userQuery = await sql.query(`select id, email, pass_word from [User] where email='${req.body.email}'`);
        const user = userQuery.recordset[0];
                
        if (user == undefined) {
            res.status(400).json({
                success: false,
                message: "Email không tồn tại, vui lòng thử lại !"
            });
        }        

        // Checking password
        const validPassword = await bcrypt.compare(req.body.password, user.pass_word);
        if (!validPassword) {
            res.status(400).json({
                success: false,
                message: "Mật khẩu sai, vui lòng thử lại"
            });
        };

        // Create and asign a access_token and refresh_token        
        const accessToken = await signAccessToken({ _id: user.id });
        const refreshToken = await signRefreshToken({ _id: user.id })

        res.status(200).header('Authorization', accessToken).json({
            success: true,
            message: 'Đăng nhập thành công',
            data: {
                "access_token": accessToken,
                "refresh_token": refreshToken
            } 
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
    
};

export const authRefreshToken = async (req, res, next) => {
    try {
        const { refreshToken } = req.body;
        if (!refreshToken) throw createError.BadRequest()
        const userId = await verifyRefreshToken(refreshToken);

        const access_token = await signAccessToken({ _id: userId });
        const refresh_token = await signRefreshToken({ _id: userId });

        res.status(200).header('Authorization', access_token).json({
            success: true,        
            data: {
                "access_token": access_token,
                "refresh_token": refresh_token
            }
        })
    } catch (error) {
        next(error)
    }
};

export const logout = async (req, res, next) => {

};