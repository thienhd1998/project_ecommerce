import Product from '../models/productModel';

// Lấy danh sách sản phẩm
export const getProduct = async (req, res, next) => {       
    try {
        // const perPage = 10;
        // const page = parseInt(req.query.page) || 0;

        // Product.find({})               
        //        .limit(perPage)
        //        .skip(perPage * page)
        //        .sort({
        //            name: 'asc'
        //        })
        //        .exec(function(error, product) {
        //            if (error) {
        //                res.status(500).json({
        //                    success: false,
        //                    message: error.message
        //                })
        //            }                   
        //             Product.estimatedDocumentCount({}).exec(function(err, count) {
        //                 if (err) {
        //                     res.status(500).json({
        //                         success: false,
        //                         message: err.message
        //                     })
        //                 }
        //                 res.status(200).json({
        //                     success: true,
        //                     data: {
        //                         items: product,
        //                         panigation: {
        //                             currentPage: page,
        //                             totalCount: count,
        //                             pageCount: Math.max(1, count / perPage),
        //                             pageSize: product.length
        //                         }
        //                     }
        //                 })
        //             })
        //        })
        const products = await Product.find({});
        res.status(200).json({
            success: true,
            data: products
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

// Thêm sản phẩm
export const createProduct = async (req, res, next) => {
    const { id, category, brand, name, images, price, inStock, sex, size, promotion, description, rating, numberReviews } = req.body;
    const product = new Product({
        id: id,
        category: category,
        brand: brand,
        name: name,
        images: images,
        price: price,
        inStock: inStock,
        sex: sex,
        size: size,
        promotion: promotion,
        description: description,
        rating: rating,
        numberReviews: numberReviews
    })
    try {
        const newProduct = await product.save();
        res.status(200).json({
            success: true,
            message: "Tạo mới sản phẩm thành công",
            data: newProduct
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Tạo mới sản phẩm thất bại"
        })
    };
};

// Sửa sản phẩm
export const updateProduct = async (req, res, next) => {
    try {   
        const updatedProduct = await Product.updateOne(
            { id: req.query.id },
            { $set: req.body }
        );
        res.status(200).json({ 
            success: true,
            message: "Cập nhật sản phẩm thành công",
            data: updatedProduct
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Cập nhật sản phẩm thất bại"
        })
    }
}

// Xoá sản phẩm
export const deleteProduct = async (req, res, next) => {
    try {
        const deletedProduct = await Product.findOneAndDelete({ id: req.query.id });
        if (deletedProduct) {
            res.status(200).json({
                success: true,
                message: "Xoá sản phẩm thành công",
                data: deletedProduct
            })
        } else {
            res.status(500).json({
                success: false,
                message: "Không tìm thấy sản phẩm để xoá"
            })
        }    
    } catch (error) {
        res.status(500).json({
            success: false,
            message: "Xoá sản phẩm thất bại"
        })
    }
};

// Lấy chi tiết sản phẩm
export const getProductDetail = async (req, res, next) => {
    try {
        const id = req.query.id;
        const productDetail = await Product.findOne({ id: id });
        console.log(`productDetail`, productDetail)
        res.status(200).json({
            success: true,
            data: productDetail
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};






