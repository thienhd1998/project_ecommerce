import express from "express";
import { verifyToken, verifyRefreshToken } from "../helpers/jwt_helper";

import { register, login, authRefreshToken, logout } from "../controllers/authController";
import { getProduct, createProduct, deleteProduct, getProductDetail, updateProduct } from "../controllers/productController";
import { getCategory, createCategory, updateCategory, deleteCategory } from '../controllers/categoryController';
import { createOrder, createOrderForAnonymous, getOrder, getOrderDetail, updateOrder, deleteOrder } from "../controllers/orderController";
import { updateAvatar } from '../controllers/userController';

const router = express.Router();

const initRoutes = app => {
    // User:
    // Login Route
    router.post('/auth/login', login);
    // Register Route
    router.post('/auth/register', register);
    // Refresh token Route
    router.post('/auth/refresh-token', verifyToken, authRefreshToken);
    // Logout Route
    router.delete('/auth/logout', verifyToken, logout);

    // Upload avatar Route
    router.post('/user/avatar', verifyToken, updateAvatar);

    // Product:
    // Product Route:
    router.get('/product/list', getProduct);
    router.post('/product/create', verifyToken, createProduct);
    router.put('/product/update', verifyToken, updateProduct);
    router.delete('/product/delete', verifyToken, deleteProduct);
    // Product Detail:
    router.get('/product/detail', getProductDetail);
    // Search product Route
    // router.get('/product/search', searchProduct);

    // Category:
    // Category list Route
    router.get('/category', getCategory);
    // Create category Route:
    router.post('/category/create', createCategory);
    // Update category Route:
    router.put('/category/update', updateCategory);
    // Delete category Route:
    router.delete('/category/delete', deleteCategory);

    // Order:
    //Get order Route:
    router.get('/order/get', verifyToken, getOrder);
    // Create order Route:
    router.post('/order/create', verifyToken, createOrder);
    router.post('/order/create-for-anonymous', createOrderForAnonymous);
    // Update order Route: 
    router.put('/order/update', verifyToken,updateOrder);
    // Delete order Route:
    router.put('/order/delete', verifyToken, deleteOrder);
    // Get order detail Route:
    router.get('/order/detail', verifyToken, getOrderDetail);

    return app.use("/", router);
};

export default initRoutes;

