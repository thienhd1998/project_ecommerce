require('dotenv').config(); 

export const dbConnection = () => {
    const config = {
        user: process.env.USER_SQL,
        password: process.env.PASSWORD_SQL,
        server: process.env.SERVER_SQL,
        database: process.env.DATABASE_NAME_SQL,
        port: 1433,
        options: {
            encrypt: true,
            enableArithAbort: true
        }
    };

    return config    
};