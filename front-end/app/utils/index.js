// Format money
export const formatNumberToCurrency = (n = 0, toFixed = 2) => {
    let reg = /(\d)(?=(\d{3})+(?:\.\d+)?$)/g

    let number = parseFloat(n).toFixed(toFixed)
    if (parseInt(n) - number == 0) {
        number = parseInt(n)
    }

    return number.toString().replace(reg, '$&.');
};

// Validate email
export const validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

// Validate phone number
const regexPhoneNumber = /^[0-9+]*$/i;
export const isPhoneNumber = value => value && regexPhoneNumber.test(value);

