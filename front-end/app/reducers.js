/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'utils/history';
import globalReducer from 'containers/App/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';

import produce from 'immer';
import { SAVE_TOKEN, SAVE_REFRESH_TOKEN, LOG_OUT } from './redux/constants';

const initialState = {
  access_token: null,
  refresh_token: null
};

const config = (state = initialState, action) => 
    produce(state, draft => {
        switch (action.type) {
            case SAVE_TOKEN:
              const { access_token } = action.payload;
              draft.access_token = access_token
            case SAVE_REFRESH_TOKEN:
              const { refresh_token } = action.payload;
              draft.refresh_token = refresh_token
            case LOG_OUT:
              draft.access_token = null;
              draft.refresh_token = null;
            default:
              return state;                
        };
    });

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    global: globalReducer,
    language: languageProviderReducer,
    router: connectRouter(history),
    config,
    ...injectedReducers,
  });

  return rootReducer;
}
