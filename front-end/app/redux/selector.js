import { createSelector } from 'reselect';

// /**
//  * Direct selector to the login state domain
//  */
const selectConfig = (state) => state.get('config').toJS();

const selectToken = () => createSelector(
    selectConfig,
    (substate) => substate.access_token
);

const selectRefreshToken = () => createSelector(
    selectConfig,
    (substate) => substate.refresh_token
);

export {
    selectToken, 
    selectRefreshToken
}