export const SAVE_TOKEN = 'actions/SAVE_TOKEN';

export const SAVE_REFRESH_TOKEN = 'actions/SAVE_REFRESH_TOKEN';

export const LOG_OUT = 'actions/LOG_OUT';

export const LOG_OUT_SUCCESS = 'acitons/LOG_OUT_SUCCESS';