import { SAVE_TOKEN, SAVE_REFRESH_TOKEN, LOG_OUT, LOG_OUT_SUCCESS } from './constants';

export const saveToken = payload => {
    return {
        type: SAVE_TOKEN,
        payload
    };
};

export const saveRefreshToken = payload => {
    return {
        type: SAVE_REFRESH_TOKEN,
        payload
    };
};

export const logOut = payload => {
    return {
        type: LOG_OUT,
        payload
    };
};

export const logOutSuccess = payload => {
    return {
        type: LOG_OUT_SUCCESS,
        payload
    };
};