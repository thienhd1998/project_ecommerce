/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import ModalAuth from '../Authen/ModalAuth/Loadable';
import Header from '../Header/Loadable';
import Footer from 'components/Footer';

import GlobalStyle from '../../global-styles';
import Favicon from 'react-favicon';

export default function App() {
  return (  
    <div>  
      <Helmet
        titleTemplate="Thiện Cosmetic"
        defaultTitle="Thiện Cosmetic"
      />
      <Favicon url="https://hasaki.vn/favicon/favicon.ico" />
      <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/features" component={FeaturePage} />
          <Route path="" component={NotFoundPage} />        
        </Switch>                       
      <ModalAuth />                
    </div>
  );
}
