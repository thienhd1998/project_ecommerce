import {
  GET_PRODUCTS,
  GET_PRODUCTS_SUCCESS,
  CLEAR_PRODUCTS
} from './constants';

export const getProduct = payload => {
  return {
    type: GET_PRODUCTS,
    payload
  }
};

export const getProductSuccess = payload => {
  return {
    type: GET_PRODUCTS_SUCCESS,
    payload
  };
};

export const clearProduct = () => {
  return {
    type: CLEAR_PRODUCTS,
    payload
  };
};