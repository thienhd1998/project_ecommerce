/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo, useState, useMemo } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import { getProduct, clearProduct } from './actions';
import { makeSelectHome }  from './selectors';
import reducer from './reducer';
import saga from './saga';
import { DataProductFake } from './DataProductFake';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Product } from '../../components/Products/index';

const key = 'home';

export function HomePage({
  home,
  dispatch
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const { loading, data } = home.product;
  const [dataProduct, setDataProduct] = useState(DataProductFake);
  
  console.log(`data`, dataProduct);
  useEffect(() => {
    dispatch(getProduct());

    return () => 
      clearProduct()
  }, [])

  useMemo(() => {
    if (!loading && !!data) {
      setDataProduct(data)
    }
    
  }, [data])

  if (!!data) {
    console.log(`data`, data);
  }


  return (
    <div className="app">
        <div className="app__container">
            <div className="grid wide">
                <div className="grid__row app__content">
                    <div className="grid__column-2">
                        <nav className="category">
                            <h3 className="category__heading">
                                <i className="category__heading-icon fas fa-list"></i>
                                Danh mục
                            </h3>
            
                            <ul className="category-list">                    
                                <li className="category-item category-item--active">
                                    <a href="#" className="category-item__link">Trang điểm mặt</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm môi</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm mắt</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm chân</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm tay</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm miệng</a>
                                </li>
                                <li className="category-item">
                                    <a href="#" className="category-item__link">Trang điểm súng</a>
                                </li>                                           
                            </ul>
                        </nav>
                    </div>
                    <div className="grid__column-10">       
                        <div className="home-filter hide-on-mobile-tablet">
                            <span className="home-filter__label">Sắp xếp theo</span>
                            <button className="home-filter__btn btn">Phổ biến</button>
                            <button className="home-filter__btn btn btn--primary">Mới nhất</button>
                            <button className="home-filter__btn btn">Bán chạy</button>

                            <div className="select-input">
                                <span className="select-input__label">Giá</span>
                                <FontAwesomeIcon className="select-input__icon" icon="angle-down" />                                

                                <ul className="select-input__list">
                                    <li className="select-input__item">
                                        <a href="#" className="select-input__link">Giá: Thấp đến cao</a>                                        
                                    </li>
                                    <li className="select-input__item">                                        
                                        <a href="#" className="select-input__link">Giá: Cao đến thấp</a>
                                    </li>
                                </ul>
                            </div>

                            <div className="home-filter__page">
                                <span className="home-filter__page-num">
                                    <span className="home-filter__page-current">1</span>
                                    /
                                    <span className="home-filter__page-total">14</span>
                                </span>

                                <div className="home-filter__page-control">
                                    <a href="#" className="home-filter__page-btn home-filter__page-btn--disabled">
                                      <FontAwesomeIcon className="home-filter__page-icon" icon="angle-left" />                                        
                                    </a>
                                    <a href="#" className="home-filter__page-btn">
                                      <FontAwesomeIcon className="home-filter__page-icon" icon="angle-right" />
                                    </a>
                                </div>                                
                            </div>
                        </div>
                        <div className="home-product">
                            <div className="grid__row home-product-item-wrapper">                                
                              {!!dataProduct && dataProduct.map((item, index) => (
                                <Product key={`prod-${index}`} item={item} />
                              ))}
                            </div>    
                        </div>

                        <ul className="pagination home-product__pagination">
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">
                                    <i className="pagination-item__icon fas fa-angle-left"></i>
                                </a>
                            </li>
                            <li className="pagination-item pagination-item--active">
                                <a href="#" className="pagination-item__link">1</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">2</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">3</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">4</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">5</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">...</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">14</a>
                            </li>
                            <li className="pagination-item">
                                <a href="#" className="pagination-item__link">
                                    <i className="pagination-item__icon fas fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  home: makeSelectHome()
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
