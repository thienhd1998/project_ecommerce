export const DataProductFake = [
    {
        id: 1,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 2,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 3,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 4,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 5,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 6,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 7,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 8,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    },
    {
        id: 9,
        name: 'Sản phẩm dưỡng tóc',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 3000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 0,         
    },
    {
        id: 10,
        name: 'Sản phẩm dưỡng thân thể',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 4000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 0,         
    },
    {
        id: 11,
        name: 'Sản phẩm dưỡng tóc quăn',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 4,
        price: 3600000,
        promotion: 20,
        quantity_sold: 10,
        isFavorite: 1,         
    },
    {
        id: 12,
        name: 'Sản phẩm dưỡng tóc xoăn',
        images: [
            'https://img.abaha.vn/photos/resized/320x/73-1574232396-myohui.png'
        ],
        star: 3,
        price: 4000000,
        promotion: 10,
        quantity_sold: 20,
        isFavorite: 1,         
    }    
]