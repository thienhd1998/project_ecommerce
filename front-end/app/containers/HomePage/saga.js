import { call, put, select, takeLatest } from 'redux-saga/effects';
import { GET_PRODUCTS } from './constants';
import { getProductSuccess } from './actions';
import { DataProductFake } from './DataProductFake';

export function* _onGetProduct() {
  try {
    yield put(getProductSuccess(DataProductFake));
  } catch (error) {
    console.log(`Loi`, error);
  };
};

export default function* defaultSaga() {
  yield [
    takeLatest(GET_PRODUCTS, _onGetProduct)
  ]
};
