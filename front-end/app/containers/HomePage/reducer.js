import produce from 'immer';
import {
  GET_PRODUCTS,
  GET_PRODUCTS_SUCCESS,
  CLEAR_PRODUCTS
} from './constants';

export const initialState = {
  product: {
    loading: false,
    data: null
  }
};

const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_PRODUCTS:
        draft.product.loading = true;
        break;
      case GET_PRODUCTS_SUCCESS: 
        draft.product.loading = false;
        draft.product.data = action.payload;
        break;
      case CLEAR_PRODUCTS:
        draft.product.loading = false;
        draft.product.data = null;
        break;
      default:
        return state;
    }
  });

export default homeReducer;
