export const REGISTER = 'app/Authen/REGISTER';

export const REGISTER_SUCCESS = 'app/Authen/REGISTER_COMPLETE';

export const CLEAR_REGISTER = 'app/Authen/CLEAR_REGISTER';