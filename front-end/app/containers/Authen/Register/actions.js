import { REGISTER, REGISTER_SUCCESS, CLEAR_REGISTER } from './constants';

export const register = payload => {
    return {
        type: REGISTER,
        payload
    }
};

export const registerCompl = payload => {
    return {
        type: REGISTER_SUCCESS,
        payload
    }
};

export const clearStateRegister = () => {
    return {
        type: CLEAR_REGISTER
    }
}