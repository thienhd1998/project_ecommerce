import React, { useEffect, useMemo, useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Register = props => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [isHiddenPass, setHiddenPass] = useState(true);
    const toggleHiddenPass = useCallback(() => 
        setHiddenPass(!isHiddenPass)
    , [isHiddenPass]);

    const [isHiddenConfirmpass, setHiddenConfirmpass] = useState(true);
    const toggleHiddenConfirmpass = useCallback(() => 
        setHiddenConfirmpass(!isHiddenConfirmpass)
    , [isHiddenConfirmpass]);

    const [error, setError] = useState({
        email: false,
        password: false,
        confirmPassword: false
    });

    const _onRegister = (e) => {
        e.preventDefault();
        const newError = {};
        const message = [];

        if (!email) {
            newError.email = true;
            message.push('Email');
        };

        if (!password) {
            newError.password = true;
            message.push('Password');
        };

        if (!confirmPassword) {
            newError.confirmPassword = true;
            message.push('Confirm password');
        };

        if (confirmPassword.trim() !== password.trim()) {
            newError.duplicatedPass = true;
            message.push('Duplicated pass');
        }

        if (message.length > 0) {
            setError(newError);
            return;
        };

        console.log(`Sap trien khai`);
    }

    return (                        
        <div>
            <div class="auth-form__container">
                <div class="auth-form__header">
                    <h3 class="auth-form__heading">Đăng ký</h3>
                    <span class="auth-form__switch-btn" onClick={e => {
                        e.preventDefault();
                        window.modalLogin.show()
                    }}>Đăng nhập</span>
                </div>

                <div class="auth-form__form">
                    <div class="auth-form__group">
                        <input type="text" class="auth-form__input" placeholder="Email của bạn" 
                            style={{ backgroundColor: error.email ? '#f8d1d3' : undefined, borderColor: error.email ? 'red' : undefined }}
                            value={email}
                            autoComplete={false}
                            onChange={e => {
                                setEmail(e.target.value)
                                setError({
                                    ...error,
                                    email: false
                                })
                            }}
                        />
                        {error.email && (
                            <span class="auth-form__input-note">* Email không được để trống</span>
                        )}                        
                    </div>
                    <div class="auth-form__group auth-form__group-pass">
                        <input type={isHiddenPass ? 'password' : 'text'} class="auth-form__input" placeholder="Mật khẩu của bạn" 
                            style={{ backgroundColor: error.password ? '#f8d1d3' : undefined, borderColor: error.password ? 'red' : undefined }}
                            value={password}
                            autoComplete={false}
                            onChange={(e) => {
                                setPassword(e.target.value)
                                setError({
                                    ...error,
                                    password: false
                                })
                            }}
                        />
                        {isHiddenPass ? (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye-slash" onClick={toggleHiddenPass} />
                        ) : (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye" onClick={toggleHiddenPass} />
                        )}
                        {error.password && (
                            <span class="auth-form__input-note">* Mật khẩu không được để trống</span>
                        )}                        
                    </div>
                    <div class="auth-form__group auth-form__group-pass">
                        <input type={isHiddenConfirmpass ? 'password' : 'text'} class="auth-form__input" placeholder="Nhập lại mật khẩu" 
                            style={{ backgroundColor: error.confirmPassword || error.duplicatedPass ? '#f8d1d3' : undefined, borderColor: error.confirmPassword || error.duplicatedPass ? 'red' : undefined }}
                            value={confirmPassword}
                            autoComplete={false}
                            onChange={(e) => {
                                setConfirmPassword(e.target.value)
                                setError({
                                    ...error,
                                    confirmPassword: false,
                                    duplicatedPass: confirmPassword == password && false
                                })
                            }}                            
                        />
                        {isHiddenConfirmpass ? (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye-slash" onClick={toggleHiddenConfirmpass} />
                        ) : (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye" onClick={toggleHiddenConfirmpass} />
                        )}
                        {error.confirmPassword && (
                            <span class="auth-form__input-note">* Xác nhận mật khẩu không được để trống</span>
                        )}
                        {!error.confirmPassword && error.duplicatedPass && (
                            <span class="auth-form__input-note">* Mật khẩu không khớp, xin vui lòng nhập lại</span>
                        )}                                                
                    </div>
                </div>

                <div class="auth-form__aside">
                    <p class="auth-form__policy-text">
                        Bằng việc đăng ký, bạn đồng ý với Shopee về
                        <a href="#" class="auth-form__text-link">Điều khoản dịch vụ</a> &
                        <a href="#" class="auth-form__text-link">Chính sách bảo mật</a>
                    </p>
                </div>

                <div class="auth-form__controls">
                    <button class="btn btn-normal auth-form__controls-back" onClick={e => {
                        e.preventDefault();
                        window.modalAuth.goBack();
                    }}>TRỞ LẠI</button>
                    <button class="btn btn--primary" onClick={e => _onRegister(e)}>ĐĂNG KÝ</button>
                </div>
            </div>

            <div class="auth-form__socials">
                <a href="#" class="auth-form__socials--facebook btn btn--size-s btn--with-icon">
                    <i class="auth-form__socials-icon fab fa-facebook-square"></i>
                    <span class="auth-form__socials-title">Kết nối với Facebook</span>
                </a>
                <a href="#" class="auth-form__socials--google btn btn--size-s btn--with-icon">
                    <i class="auth-form__socials-icon fab fa-google"></i>
                    <span class="auth-form__socials-title">Kết nối với Google</span>
                </a>
            </div>
        </div>            
    )
};

export default Register;