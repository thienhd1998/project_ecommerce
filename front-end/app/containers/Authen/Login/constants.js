export const LOGIN = 'app/Authen/LOGIN';

export const LOGIN_SUCCESS = 'app/Authen/LOGIN_SUCCESS';

export const CLEAR_LOGIN = 'app/Authen/CLEAR_LOGIN';