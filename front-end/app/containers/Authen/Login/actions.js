import { LOGIN, LOGIN_SUCCESS, CLEAR_LOGIN } from './constants';

export const login = payload => {
    return {
        type: LOGIN,
        payload
    }
};

export const loginSuccess = payload => {
    return {
        type: LOGIN_SUCCESS,
        payload
    }
};

export const clearStateLogin = () => {
    return {
        type: CLEAR_LOGIN        
    }
};