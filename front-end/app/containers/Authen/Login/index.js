import React, { useEffect, useMemo, useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Login = props => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isHidden, setHidden] = useState(true);
    const toggleHidden = useCallback(() => 
        setHidden(!isHidden)
    , [isHidden]);

    const [error, setError] = useState({
        email: false,
        password: false
    })

    const _onLogin = (e) => {
        e.preventDefault();
        const newError = {};
        const message = [];

        if (!email) {
            newError.email = true;
            message.push('Email')
        }

        if (!password) {
            newError.password = true;
            message.push('Password')
        }

        if (message.length > 0) {
            setError(newError)
            return;
        }

        console.log(`sap trien khai`);
    }

    return (        
        <div>
            <div class="auth-form__container">
                <div class="auth-form__header">
                    <h3 class="auth-form__heading">Đăng nhập</h3>
                    <span class="auth-form__switch-btn" onClick={e => {
                        e.preventDefault();
                        window.modalRegister.show()
                    }}>Đăng ký</span>
                </div>

                <div class="auth-form__form">
                    <div class="auth-form__group">
                        <input type="text" class="auth-form__input" placeholder="Email của bạn"
                            style={{ backgroundColor: error.email ? '#f8d1d3' : undefined, borderColor: error.email ? 'red' : undefined }}
                            value={email}
                            autoComplete={false}
                            onChange={(e) => {
                                setEmail(e.target.value)
                                setError({
                                    ...error,
                                    email: false
                                })
                            }}
                        />
                        {error.email && (
                            <span class="auth-form__input-note">* Email không được để trống</span>
                        )}                        
                    </div>
                    <div class="auth-form__group auth-form__group-pass">
                        <input type={isHidden ? 'password' : 'text'} class="auth-form__input auth-form__input-pass" placeholder="Mật khẩu của bạn" 
                            style={{ backgroundColor: error.password ? '#f8d1d3' : undefined, borderColor: error.password ? 'red' : undefined }}
                            value={password}
                            autoComplete={false}
                            onChange={(e) => {
                                setPassword(e.target.value)
                                setError({
                                    ...error,
                                    password: false
                                })
                            }}
                        />
                        {isHidden ? (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye-slash" onClick={toggleHidden} />
                        ) : (
                            <FontAwesomeIcon className="auth-form__input-pass-icon" icon="eye" onClick={toggleHidden} />
                        )}                                                
                    </div>
                    {error.password && (
                        <span class="auth-form__input-note">* Mật khẩu không được để trống</span>
                    )}                    
                </div>

                <div class="auth-form__aside">
                    <div class="auth-form__help">
                        <a href="#" class="auth-form__help-link auth-form__help-forgot">Quên mật khẩu</a>
                        <span class="auth-form__help-separate"></span>
                        <a href="#" class="auth-form__help-link">Cần trợ giúp?</a>
                    </div>
                </div>

                <div class="auth-form__controls">
                    <button class="btn btn-normal auth-form__controls-back" onClick={e => {
                        e.preventDefault();
                        window.modalAuth.goBack();
                    }}>TRỞ LẠI</button>
                    <button class="btn btn--primary" onClick={e => _onLogin(e)}>ĐĂNG NHẬP</button>
                </div>
            </div>

            <div class="auth-form__socials">
                <a href="#" class="auth-form__socials--facebook btn btn--size-s btn--with-icon">
                    <i class="auth-form__socials-icon fab fa-facebook-square"></i>
                    <span class="auth-form__socials-title">Kết nối với Facebook</span>
                </a>
                <a href="#" class="auth-form__socials--google btn btn--size-s btn--with-icon">
                    <i class="auth-form__socials-icon fab fa-google"></i>
                    <span class="auth-form__socials-title">Kết nối với Google</span>
                </a>
            </div>
        </div>            
    )
};

export default Login;