import React, { useEffect, useMemo, useCallback, useState } from 'react';
// import { connect } from 'react-redux';
// import { compose } from 'redux';
// import { createStructuredSelector } from 'reselect';

import ModalLogin from '../Login/Loadable';
import ModalRegister from '../Register/Loadable';

const ModalAuth = props => {
    const [index, setIndex] = useState(0);

    const [visible, setVisible] = useState(false);
    const toggleVisible = useCallback(() => 
        setVisible(!visible)
    , [visible])

    useEffect(() => {
        window.modalLogin = {
            show: () => {
                setIndex(0);
                toggleVisible()                
            }
        }
    }, []);

    useEffect(() => {
        window.modalRegister = {
            show: () => {
                setIndex(1);
                toggleVisible()                
            }
        }
    }, []);

    useEffect(() => {
        window.modalAuth = {
            goBack: () => {                
                setVisible(false)
            }
        }
    }, []);

    return (
    <div>
        {visible && (
            <div class="modal" onClick={(e) => {
                e.preventDefault();                
                toggleVisible()
            }}> 
                <div class="modal__overlay"></div>
    
                <div class="modal__body">
                    <div className="auth-form" onClick={e => {
                        e.preventDefault()
                        e.stopPropagation()
                    }}>
                        {index == 0 && <ModalLogin />}
                        {index == 1 && <ModalRegister />}           
                    </div>     
                </div>
            </div>
        )}
    </div>        
    )
};

export default ModalAuth;