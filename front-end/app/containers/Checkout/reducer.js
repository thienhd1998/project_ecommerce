import produce from 'immer';

import { DEFAULT_ACTION, ADD_ITEM, CLEAR_CART, CLEAR_ITEM_FROM_CART, REMOVE_ITEM } from './constants';
import { addItemToCart, removeItemFromCart } from './cartUltils';

export const initialState = {
    cartItems: []
};

const cartReducer = (state = initialState, action) => 
    produce(state, draft => {
        switch (action.type) {
            case DEFAULT_ACTION:
                draft.cartItems = [];
                break;            
            case ADD_ITEM:
                draft.cartItems = addItemToCart(state.cartItems, action.payload)
            case REMOVE_ITEM:
                draft.cartItems = removeItemFromCart(state.cartItems, action.payload)
            case CLEAR_ITEM_FROM_CART:
                draft.cartItems = state.cartItems.filter(cartItem => cartItem.id != action.payload.id)
            case CLEAR_CART:
                draft.cartItems = []
            default:
                break;
        };
    });

export default cartReducer;