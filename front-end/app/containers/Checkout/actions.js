import { DEFAULT_ACTION, ADD_ITEM, REMOVE_ITEM, CLEAR_ITEM_FROM_CART, CLEAR_CART } from './constants';

export const defaultAction = () => {
    return {
        type: DEFAULT_ACTION
    }   
};

export const addItem = payload => {
    return {
        type: ADD_ITEM,
        payload
    };
};

export const removeItem = payload => {
    return {
        type: REMOVE_ITEM,
        payload
    }
};

export const clearItemFromCart = payload => {
    return {
        type: CLEAR_ITEM_FROM_CART,
        payload
    }
};

export const clearCart = payload => {
    return {
        type: CLEAR_CART,
        payload
    };
};