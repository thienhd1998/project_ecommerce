export const DEFAULT_ACTION = 'app/Cart/DEFAULT_ACTION';
export const ADD_ITEM = 'app/Cart/ADD_ITEM';
export const REMOVE_ITEM = 'app/Cart/REMOVE_ITEM';
export const CLEAR_ITEM_FROM_CART = 'app/Cart/CLEAR_ITEM_FROM_CART';
export const CLEAR_CART = 'app/Cart/CLEAR_CART';