const fetchAPI = (url, options, access_token) => {
    const lastOptions = Object.assign({}, options, {
        mode: 'cors',
        headers: new Headers({
            'Accept': 'application/json',            
            'Content-Type': 'application/json',
            Authorization: !!access_token ? `Bearer ${access_token}` : undefined,
        })
    });
    return new Promise((resolve, reject) => {
        fetch(`${process.env.URL_API}:${process.env.PORT}/${url}`, lastOptions)
            .then(response => response.json())
            .then((res) => {
                if (res.status == 401) {

                }
                else if (!res) {
                    const error = new Error("Dữ liệu không hợp lệ!");                    
                    if (!!window.alertCustom) {
                        window.alertCustom.error('Dữ liệu không hợp lệ!')
                    }
                    reject(error)
                }
                resolve(res)
            })
    })
};

export default fetchAPI;