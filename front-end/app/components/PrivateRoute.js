import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = props => {
    const { access_token, ...rest } = props;
    if (!access_token) {
        <Redirect to="/login" />
    }
    return <Route {...rest} />
};

export default PrivateRoute;