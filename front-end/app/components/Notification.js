import React, {useEffect} from 'react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notification = () => {
    useEffect(() => {
        window.alertCustom = {
            success: (message, title = '') => {
                NotificationManager.success(message, title, 3000);
              },
              error: (message, title = '') => {
                NotificationManager.error(message, title, 3000);
              },
              warning: (message, title = '') => {
                NotificationManager.warning(message, title, 3000);
              }
        }
    }, []);

    return (
        <NotificationContainer />
    )
};

export default Notification;