// import the library
import { library } from '@fortawesome/fontawesome-svg-core';

// import your icons
import { 
  faCode, 
  faHighlighter, 
  faEye, 
  faEyeSlash,
  faSearch, 
  faAngleDown,
  faShoppingCart,
  faCheck,
  faBell,
  faQuestionCircle,
  faAngleLeft,
  faAngleRight,
  faHeart,  
  faStar, 
} from '@fortawesome/free-solid-svg-icons';

import { far } from  '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

library.add(
  far,
  fas,
  faCode,
  faHighlighter,
  faEye,
  faEyeSlash,
  faSearch,
  faAngleDown,
  faShoppingCart,
  faCheck,
  faBell,
  faQuestionCircle,
  faAngleLeft,
  faAngleRight,
  faHeart,
  faStar
  // more icons go here
);