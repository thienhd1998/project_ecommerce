import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatNumberToCurrency } from '../../utils/index';

export const Product = (props) => {
    const { id, name, images, star, price, promotion, quantity_sold, isFavorite } = props.item;
    console.log(`pro`, props);
    return (
        <div className="grid__column-2-4">                                    
            <div className="home-product-item">                                        
                <div className="home-product-item__img" style={{ backgroundImage: `${images[0]}` }}></div>
                <h4 className="home-product-item__name">{name}</h4>
                <div className="home-product-item__price">
                    <span className="home-product-item__price-old">{formatNumberToCurrency(price)}đ</span>
                    <span className="home-product-item__price-current">{formatNumberToCurrency(price - (price * promotion) / 100)}đ</span>
                </div>
                <div className="home-product-item__action">
                    <span className="home-product-item__like">
                        <FontAwesomeIcon className="home-product-item__like-icon-empty" icon={["far", "heart"]} />
                        <FontAwesomeIcon className="home-product-item__like-icon-fill" icon="heart" />                                                
                    </span>
                    <div className="home-product-item__ratting">
                        {[1, 2, 3, 4, 5].map((item, index) => (
                            star >= item ? (
                                <FontAwesomeIcon key={`star-${index}`} className="active" icon="star" />
                            ) : (
                                <FontAwesomeIcon key={`star-${index}`} icon={["far", "star"]} />
                            )
                        ))}                                                                                                
                    </div>                         
                    <span className="home-product-item__sold">{quantity_sold} đã bán</span>           
                </div>
                <div className="home-product-item__origin">
                    <span className="home-product-item__brand">Whoo</span>
                    <span className="home-product-item__origin-name">Nhật Bản</span>
                </div>
                {isFavorite == 1 && (
                    <div className="home-product-item__favourite">
                        <FontAwesomeIcon icon={["fas", "check"]} />
                        <span>Yêu thích</span>
                    </div>
                )}
                <div className="home-product-item__sale-off">
                    <span className="home-product-item__sale-off-percent">{promotion}%</span>
                    <p className="home-product-item__sale-off-label">GIẢM</p>
                </div>
            </div>                                                                        
        </div>
    )
};